﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UnityListExtensions {

    public static void RemoveAndDestroy<T> (this List<T> list, T behaviour) where T: MonoBehaviour {
        list.Remove (behaviour);
        GameObject.Destroy (behaviour.gameObject);
    }
}
